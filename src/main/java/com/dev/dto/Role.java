package com.dev.dto;

public enum Role {
    USER,
    ADMIN
}
